﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="11008008">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueCircle</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">1179848</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">9341183</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate_4x4x4</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,J!!!*Q(C=T:;`&lt;B."%)@H%II5#/3/DMD+!S".B93IX&amp;`FHGJ[+C-B*6O[1++?6\$]"G[AHS[V&amp;?5&amp;X+2!(!&lt;T?&lt;X_=Y[$GS"FTX-Z`X[TM^`NLF=2S?V-Z*EMDL7,UHCE4Y\N1^N@30YM6I&lt;MF#A0O?NC@4PM\T`&gt;[\]?_I"`K*&amp;]@3U0N5@RN[WJG_:BP[G8%^8+W0&amp;8\F\'RN_[L9S.8\@]_8W`W6;9T_@V!&lt;^ET/=0_G1U]X`[R8Z[@N0SGW9](L@HP[YX0K]ZTKW^@P8+XX(XVT^H8)SX^P\_E,LFXP&gt;J/_Z"P^7/_U=;/?FIUH^O)N8[&amp;RW%%U&lt;I[LA)^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%.8&gt;%68&gt;%68&gt;&amp;W?*MM,8&gt;!&amp;843@--(AQ5""U;"!E!S+",?!*_!*?!)?PEL!%`!%0!&amp;0Q%/+"$Q"4]!4]!1]&gt;*/!*_!*?!+?A)&gt;3G34SB1Z0Q%.Z=8A=(I@(Y8&amp;Y'&amp;)=(A@!'=QJ\"1"1RT4?8"Y("[(BU&gt;R?"Q?B]@B=8CQR?&amp;R?"Q?B]@BI5O?&amp;=]USQM&gt;(MK)Q70Q'$Q'D]&amp;$;4&amp;Y$"[$R_!R?"B/$"[$RY!Q"D3+AS"'*S0"_',Q'$T]%90(Y$&amp;Y$"[$"SOPE/7:7&gt;)M,X2Y&amp;"[&amp;2_&amp;2?"1?3ID#I`!I0!K0QE.:58A5(I6(Y6&amp;Y'%I5(I6(Y6&amp;!F%%:8J2C3E=F32%5(DZZNWB?*=]EGK`SUVRN6#E&lt;5-L'EL*BJ'Q%+1MM:?'E,)C5C:9SA6)G2MI,3XE2+9"3"J:35%KCT,B0C1ER)I&lt;%A/A40;*,&gt;*:&gt;(TFR.JP*&gt;$K6S71CI^&amp;)BM/B$!9$[@@\UOPVJ.PN3K@4W:R7ZVSL6KX0J@4RV&gt;@U?8"T^_MN=5FU&lt;O\1PZ#TDH4SL5JH&lt;[JU`LR+[&lt;2+1HRY5;60\[NU_;&gt;+6Q&gt;CM&lt;B.LX``4#^`X+&lt;4G_^*LODXDJ!=KX0J+:S.=L,[FWE\2X]"J&amp;:7@A!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&lt;_5F.31QU+!!.-6E.$4%*76Q!!%LQ!!!2#!!!!)!!!%JQ!!!!C!!!!!2V0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O&lt;(:D&lt;'&amp;T=Q!!!!!!C"%!A!A!-!!!#!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"7A-G]L.0;1LO0NB?:DVN@!!!!$!!!!"!!!!!!NG4&lt;$'4ORU/U]N_CLG^3F^1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!"#^QG*B'1JK4$A;JR@3-][@!!!!%$"@,#PBG41QY)#"A?7_2TY!!!"D!!&amp;-6E.$.U^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T/E^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZD&gt;'Q!!!!!!!%!!F:*4%)!!!!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!(Q!!!"JYH'0A:G!39""AN'!!EE#7!9DG!')'!!Y!!2%!!!!!1Q!!!32YH'.AQ!4`A1")-4)Q-,U$UGRIYG!;RK9W1$98GRWY\-5OTAA59XI!:,!!-20#8S$!&gt;!?)XW$IQ')W!*:[+)I!!!!!2A!"6EF%5T&gt;0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O&lt;(:D&lt;'&amp;T=TJ0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O9X2M!!!!!!!!!!-!!!!!!/!!!!&amp;Q?*S\Q=$!E'FM93&lt;!S-$!T!!"S@EJK3!["CDG$"5T0(!94-0YBW&amp;KI84T'R&lt;$3]V(7&amp;Y_`P``0Z$`'#(?\;,#U6!BT]$@+A95/.\AQAC2[G22?1&amp;7=I4FM/%"%!HE#%-FA:JYOHV57$I&lt;L:D"0)(/2DMAKYOBOR-EUM6J=:#`F1WI=A&gt;)RW%A&lt;]I"`GVA6XZ'W.P.=&gt;R2*!&amp;EL;0'$$$FY!'G8'[AO1")@@L^H\%?3/MQ-$)Q!EV6!^&amp;!0DM$%^B.D!QM$%]98D.]9PD*1!RQ^H&gt;R2?;$QB-!EG2%?Q!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$"%!A!A!!!1R-3YQ!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$"%!A!A!!!1R-3YQ!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y"R')_!?X@@]`MZX`0\@N`T_R(@]````YQ!!!'-!!!"D!1!!9!,Q!'!%BA"A#)$A9"#!9'!AA/"A9)'A9(S"I'"`AS"A@`9A9(`])'"``#"A@`]A9(`^Y'"``%"A@`S!9(`^A'!`_1"A"`I!9!"]!'!!#!"A!!!!@````]!!!)!``````````````````````!!!!!0`Q!0!!`Q!0!!```Q!!!!$``Q`Q``$```$`````]!````]0]!``!0`Q``````!0````$`$```]0]0`````Q$````Q`Q!0!!``$`````]!``````````````````!!)A!!!!!!!!!!!!!!!!`Q!#)!!!!!!!!!!!!!!!!0]!!C!!!!QMQ!!!!!!!!!$`!!!!!!$/:G9MQ!!!!!!!`Q!!!!!-*G:G:G,-!!!!!0]!!!!!QG:G:G:G:OU!!!$`!!!!!#:G:G:G:G:C!!!!`Q!!!!ZG:G:G:G:GZA!!!0]!!!$G:G:G:G:G&lt;G9!!!$`!!!!\OZG:G:G:O:G!!!!`Q!!!/:G&lt;O:G:GZG:A!!!0]!!!!G:G:G\G&lt;G:G9!!!$`!!!!*G:G:G:C:G:G!!!!`Q!!!#:G:G:G&lt;G:G9A!!!0]!!!!G:G:G:GZG:G)!!!$`!!!!*G:G:G:O:G:C!!!!`Q!!!#:G:G:G&lt;G:G&lt;!!!!0]!!!!G:G:G:GZG:M!!!!$`!!!!:G:G:G:O:G)!!!!!`Q!!!-XG:G:G&lt;G9A!!!!!0]!!!!!$.ZG:GZCQ!!!!!$`!!!!!!!!T?:O&lt;!!!!!!!`Q!!!!!!!!!-UM!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`````````````````````]!!!1!````````````````````````````````````````````*#1E*#1E*#4```]!!!$`!!!!``]!!!$`!!!!``````]E*#1E*#1E*0````]!``]!````!0``````!0```````````S1E`````````Q$``Q!!````!!$```]!````````````*#4`````````!0``!0```````Q$``Q$```````````]E*0````````]!``]!!!$`!!!!````!0```````````S1E````````````````````````````````````!!!!U^-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!$4UQ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!.04!!!!!!!!^K.Z_!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!0&lt;(?++=RJV6^A!!!!!!!!!!!!!!``]!!!!!!!!!!!$WIXBYIHBY?(D'IXEL!!!!!!!!!!$``Q!!!!!!!!!!^K.Y?(CC?(BY?(BY?*T(A!!!!!!!!0``!!!!!!!!!!#D?(BY?+*Y?(BY?(BY?*T0!!!!!!!!``]!!!!!!!!!J(BY?(BYIHBY?(BY?(C=R]9!!!!!!!$``Q!!!!!!!+7=?(BY?(CC?(BY?(BYH-&gt;YRA!!!!!!!0``!!!!!!!!R]@(RZRY?+*Y?(BY?(D(H(D'!!!!!!!!``]!!!!!!!$(H*S=RM@(RHBY?(BYRZRY?-9!!!!!!!$``Q!!!!!!!+/=H*S=H*T'R]@'?-?=?(BYRA!!!!!!!0``!!!!!!!!IZS=H*S=IJS=H-&lt;/H(BY?(D'!!!!!!!!``]!!!!!!!#DH*S=H++=H*S=H-?=?(BY?+-!!!!!!!$``Q!!!!!!!+/=H*SCH*S=H*S=R[+CIHBYIQ!!!!!!!0``!!!!!!!!IZS=IJS=H*S=H*T(H(CCIK+D!!!!!!!!``]!!!!!!!#DH++=H*S=H*S=H-?=?(BYH(E!!!!!!!$``Q!!!!!!!+/CH*S=H*S=H*S=RZRY?*RZ!!!!!!!!!0``!!!!!!!!IJS=H*S=H*S=H*T(H(C=IQ!!!!!!!!!!``]!!!!!!!$YA-?CH*S=H*S=H-&gt;Y?+-!!!!!!!!!!!$``Q!!!!!!!!!!!0C!R];=H*S=RXCD^A!!!!!!!!!!!0``!!!!!!!!!!!!!!!!_(L(RJT(RCM!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!L?KML!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!)!!1!!!!!!2A!"2F")5$&gt;0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O&lt;(:D&lt;'&amp;T=TJ0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O9X2M!!!!!!!!!!-!!!!!!X)!!!C]?*SN6EV0%V%5P;_UZB5RP%%+V)$^]"6*B%222"#UA5'$10E3!S\5RK*AC$55$#P94%QQ958#QI1N7R=M=-(#G-:..[R=S+,B(_C#;#,4]&lt;[:ND0^3+P2,F[;ZJR\\LXHP*=#U!_MQ:;#625)/]9PQSJ52B)%).Z')@VB5_18E&amp;IX57'54L&amp;$7YIUKF!63444S`)[@%N$39?NFRUB^"2T9[6+&amp;;IDC8LJ(E]S`L[2LTMS*36I9BME::PGHB^U1YGC'CANYJ4;3!K)\,0&lt;&amp;@^9_/7MQM7PTD&lt;KVEM[67"SIGG2*S^C2:4_J*?UN2+/UOG3A#6&lt;9(&gt;XVS2*"KF:&lt;_-'=AD8WWUNQ;F"4IQH7X6/J=Z"H9[-DOR*&lt;9L?"3G@?F:/-+1C&lt;S7T'J\2EDVKM#BP4I6;HOSD&lt;HI5_?IY-[\M!1%3@USV07V:]+71=%((VK!2A7\#*P(\J!J8F)4N#&gt;B23UOB7"^M[D&lt;9-T:=&amp;4&lt;=U7VQSOM-4"]WS`AAW_0_JP[&amp;Z&gt;D3\+)X_MT\&gt;#%=CXF@,=[`$C`.?C0BJ&lt;$B%!M9P3%,?!P")1*7U[\*#8?H7)D1.`,C!DM%)7AV)!I\/TOY%TR.;A&gt;3[XESSZ.1419Z;U,%8+:IR&amp;TG&gt;6RG9!9#$U(_?@_Z7'OAWW:*MGQGO1O4,,(]TH+3@00`*,H-HHIQ=[NZY99?O!1D*9,;;Z$-=#.?M(J+='YB:]U;&lt;O1A)[.4+NSX#])NO(HBXNL;SA^X-/O(%8%\)5&lt;%F10N2$M214`2DK%,2CV"(U7@5%]E6PO/3IWIF/0,C0$FLO[,SYSW!YU*F)WWYA^&amp;#Z[9+B6#',FWHHSAF[T73^&lt;""@""/UZW@ADLC&amp;0S'&amp;?-J#K]=9_RC..1L8BVC%_=T*^?ZAPK&amp;_5,I2&amp;.10%M$`XXKE[0/?-9TFD(ES\T7DGA'4JRQ0X^@2Q1TW)$OH!(8BXC%W?W`HRB+Q:5"-%HTP,1@[^K(8$=?(*G,#;+'4'KVFA8G`%=LM'L1XTCT%L)B&gt;U5B9I,^W@)PS^KH8$#G($;-G'6=2E,*C3Z#B[%&amp;'NG,E=#\_S1Z1W&gt;="JP;-7&lt;2])&amp;")9M$QQ^`,S=`&lt;^!*\GMF1+Q!;S`JI*,\K`ZS!:&amp;BX31$L"N@)`R;;[DW`1Y`C8\JS)_H'5K"`#7L0"X(NLQ'W1*4;5!!!!!!!1!!!!J!!!!2A!"1E2)5$&gt;0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O&lt;(:D&lt;'&amp;T=TJ0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O9X2M!!!!!!!!!!-!!!!!!'5!!!"V?*RD9'!I&amp;*"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```V=J9O4Y?O1;8.%2(TB4::9]BQ1!:1A:GA!!!!!!!!1!!!!(!!!#1Q!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S%1#!#!!!!!%!#!!Q`````Q!"!!!!!!!Y!!!!!A!+1#%%4G^O:1!!*E"1!!%!!"V0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O&lt;(:D&lt;'&amp;T=Q!"!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S%1#!#!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ%1#!#!!!!!%!"1!(!!!"!!$(U'*6!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N="%!A!A!!!!"!!5!"Q!!!1!!R^"C61!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-2!)!)!!!!!1!)!$$`````!!%!!!!!!$1!!!!#!!J!)12/&lt;WZF!!!C1&amp;!!!1!!'&amp;2F&lt;8"M982F6'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q!!!1!"!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G52!)!)!!!!!1!&amp;!!-!!!%!!!!!!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B%1#!#!!!!!)!#E!B"%ZP&lt;G5!!#*!5!!"!!!96'6N='RB&gt;'65:8.U1W&amp;T:3ZM&gt;G.M98.T!!!"!!%!!!!!!!!!!!!%!!-!$1!!!!1!!!"3!!!!+!!!!!)!!!1!!!!!*A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%-!!!"OXC=D5_\4M.!%"T\,M1RBI2XAB4JUF"1U0!$FJ!IQ5,19_RT:'4:E?]35@*XN0Q+@]"Q.K+!!IVUOTO\/\-(Y"2BP*!X4;W":XS]P4]^!0$HNSN&lt;.L623&gt;K;MF[K?WWMO;AW7:5;-`O\G^E+ZX%#$ZB@67ND&gt;;O;1LE6N7L,47KVSF/&lt;9E!A2%YH#:_&amp;[%N=1C,!)\/)4/+;7TD\FWK)D(O#E[.Y#&amp;&amp;53Q4RKWDO&lt;%@IJM#%3I+4WYD%/C`INE$EV(?&amp;@=F\TRW-?T70'*"R:`$B&amp;&gt;VZ%N==6&gt;D(!&gt;N@/#4X$&lt;`(&lt;_;H=_41@&gt;P$=7]IG5P'-;0!E'YD4,#(%UT*4D%D%Q#@3$N#@1!!!(=!!1!#!!-!"1!!!&amp;A!$Q1!!!!!$Q$9!.5!!!"B!!]%!!!!!!]!W!$6!!!!;A!0"!!!!!!0!.A!V1!!!(/!!)1!A!!!$Q$9!.5!!!"VA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4)"-!"35V*$$1I!!UR71U.-1F:8!!!3P!!!"%)!!!!A!!!3H!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%^#5U=!!!!!!!!"H%.$5U=!!!!!!!!"M%R*&gt;GE!!!!!!!!"R%.04F!!!!!!!!!"W&amp;2./$!!!!!!!!!"\%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!!!!!#+(:F=H-!!!!%!!!#0%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$1!!!!!!!!#S'FD&lt;$A!!!!!!!!#X%.11T)!!!!!!!!#]%R*:H!!!!!!!!!$"%:13')!!!!!!!!$'%:15U5!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!`````Q!!!!!!!!#U!!!!!!!!!!$`````!!!!!!!!!-A!!!!!!!!!!0````]!!!!!!!!!X!!!!!!!!!!!`````Q!!!!!!!!$Q!!!!!!!!!!$`````!!!!!!!!!6A!!!!!!!!!!0````]!!!!!!!!"9!!!!!!!!!!!`````Q!!!!!!!!'%!!!!!!!!!!$`````!!!!!!!!!=Q!!!!!!!!!!0````]!!!!!!!!#'!!!!!!!!!!%`````Q!!!!!!!!,]!!!!!!!!!!@`````!!!!!!!!!R!!!!!!!!!!#0````]!!!!!!!!$)!!!!!!!!!!*`````Q!!!!!!!!-U!!!!!!!!!!L`````!!!!!!!!!U1!!!!!!!!!!0````]!!!!!!!!$7!!!!!!!!!!!`````Q!!!!!!!!.M!!!!!!!!!!$`````!!!!!!!!!`!!!!!!!!!!!0````]!!!!!!!!&amp;^!!!!!!!!!!!`````Q!!!!!!!!HY!!!!!!!!!!$`````!!!!!!!!#A!!!!!!!!!!!0````]!!!!!!!!+4!!!!!!!!!!!`````Q!!!!!!!!X%!!!!!!!!!!$`````!!!!!!!!$=Q!!!!!!!!!!0````]!!!!!!!!/'!!!!!!!!!!!`````Q!!!!!!!![%!!!!!!!!!!$`````!!!!!!!!$IQ!!!!!!!!!!0````]!!!!!!!!1V!!!!!!!!!!!`````Q!!!!!!!"$=!!!!!!!!!!$`````!!!!!!!!%/1!!!!!!!!!!0````]!!!!!!!!2%!!!!!!!!!#!`````Q!!!!!!!")A!!!!!"F0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2V0=(2J&lt;WZT)&amp;"B=H.J&lt;G=A6'6T&gt;(-O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!!A!"!!!!!!!!!1!!!!)!#E!B"%ZP&lt;G5!!!A!5!!"!!!!!1!"!!!!!!!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!ABA!)!!!!!!!!!!!!!!1!!!!!!!!!!!!!#!!J!)12/&lt;WZF!!!)!&amp;!!!1!!!!%!!1!!!!(````_!!!!!!%16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!%1#!#!!!!!!!!!!!!!%!!!!96'6N='RB&gt;'65:8.U1W&amp;T:3ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"H!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!"*!!!!"A=]&gt;GFM;7)_"G&amp;E:'^O=QV@3EN*)&amp;2P&lt;WRL;82T#6:*)&amp;2F=X2F=AR5:8.U1W&amp;T:3ZM&lt;')16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Options Parsing Tests.ctl" Type="Class Private Data" URL="Options Parsing Tests.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="setUp.vi" Type="VI" URL="../setUp.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="tearDown.vi" Type="VI" URL="../tearDown.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!*!!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
	</Item>
	<Item Name="testExample.vit" Type="VI" URL="../testExample.vit">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="temp_VI_UnderTest.vi" Type="VI" URL="../temp_VI_UnderTest.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$*!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].97.U&gt;7&amp;M)(*F=X6M&gt;!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!6!$Q!!Q!!Q!%!!5!"!!%!!1!"!!%!!9!"!!%!!1#!!"Y!!!.#!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!!!1!(!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
	</Item>
	<Item Name="testBasicOptionExtraction.vi" Type="VI" URL="../testBasicOptionExtraction.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="testGet Trailing Arguments.vi" Type="VI" URL="../testGet Trailing Arguments.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777224</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="test Option as Flag Just Has Set.vi" Type="VI" URL="../test Option as Flag Just Has Set.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777224</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="test Default Value Returned If Not Set.vi" Type="VI" URL="../test Default Value Returned If Not Set.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777224</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="test Works On Secondary Tag Values.vi" Type="VI" URL="../test Works On Secondary Tag Values.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777224</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="test Works With Alternative Label Marker.vi" Type="VI" URL="../test Works With Alternative Label Marker.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test Detects Missing Required Options.vi" Type="VI" URL="../test Detects Missing Required Options.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="test No Missing Flag If All Options Are Present.vi" Type="VI" URL="../test No Missing Flag If All Options Are Present.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(5^Q&gt;'FP&lt;H-A5'&amp;S=WFO:S"5:8.U=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;4X"U;7^O=S"198*T;7ZH)&amp;2F=X2T,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
</LVClass>
